////////___________________________________________
/// CONSTANTS		///////////////////////////////
const express = require('express');
const app = express();

// Change this to set the listening port for the app
const portNumber = 8080;
// Debug statements should be wrapped in 
const DEBUG = false;


////////___________________________________________
/// VARIABLES		///////////////////////////////
var Pool = require('pg').Pool;
var config = {
	host: 'localhost',
	user: 'viewer',
	password: '4HFud8fC5k83NbD0',
	database: 'world',
};
//var pool = new Pool(config);


// Initialize
app.set('port', (portNumber));
app.use(function (req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	next();
});


// Connect the front-end
console.log('running production server');
app.use(express.static('client/build'));

// INTERNAL
function login (user, password) {
	bcrypt.hash(user, 10, function(err, hash) {
		var exists = execute_query("select username from users where username=$1 and p=$2;", [user, hash]);
		
		if (exists.rows > 0) {
			execute_query("update table users set loggedin=true where user=$1;", [user]);
		}
	});
}
function logout (user, password) {
	bcrypt.hash(user, 10, function(err, hash) {
		var exists = execute_query("select username from users where username=$1 and p=$2;", [user, hash]);
		
		if (exists.rows > 0) {
			execute_query("update table users set loggedin=false where user=$1;", [user]);
		}
	});
}
function addUser (user, password) {
	bcrypt.hash(user, 10, function(err, hash) {
		execute_query("insert into users values ($1, $2);", [user, hash]);
	});
}

async function execute_query(query, args) {
	try {
		var response = await pool.query(query, args);
	} catch (e) {
		console.error(e);
	}
}

// GETs
app.get('/find', async (req, res) => {
	try {
		
		// Send the client a dummy value
		res.json({'one': {'item':'things'}, 'two': {'item':'things'}});
		
		// print debug statment
		if (DEBUG) {
//			console.log(JSON.stringify(reply.rows[0], reply.rows.length));
		}
		
		// send the results to the client
//		res.json(reply.rows);
	} catch(e) {
		console.error('ERROR: ' + e);
		res.json({'error': "invalid inputs given"});
	}
});

// POSTs
app.post('/csv', async (req, res, next) => {
	let uploadFile = req.files.file;
	const fileName = req.files.file.name;
	
	uploadFile.mv(
		`${__dirname}/public/files/${fileName}`,
		function (err) {
			if (err) return res.status(500).send(err);
			
			res.json({
				file: `public/${req.files.file.name}`,
			})
		},
	)
});

app.post('/user', async (req, res) => {
	if (!req.body.username || !req.body.p) {
		res.json ({'error': "Missing a parameter"});
	} else {
		try {
			var reply = await addUser(req.body.user, req.body.p);

			res.json (reply.rows);
		} catch(e) {
			console.error('ERROR: ' + e);
			res.json ({'error': "error when entering information"});
		}
	}
})
app.post('/login', async (req, res) => {
	if (!req.body.username ) {
		res.json ({'error': "Enter your username"});
	} else if (!req.body.p) {
		res.json ({'error': "Enter your password"});
	} else {
		try {
			var reply = await pool.query(req.body.user, req.body.p);

			res.json (reply.rows);
		} catch(e) {
			console.error('ERROR: ' + e);
			res.json ({'error': "error when entering information"});
		}
	}
})


// Listen
app.listen(app.get('port'), () => {
	console.log('Running on port ' + portNumber);
});
