var React = require('react');
var PropTypes = require('prop-types');
var axios = require('axios');



class Login extends React.Component {
    constructor(props){
      super(props);
      this.state = {
        username: '',
        password: '',
        create: false,
        msg: ''
      } 
      this.handleChange = this.handleChange.bind(this);
      this.handlePageChange = this.handlePageChange.bind(this);
      this.handlePasswordChange = this.handlePasswordChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
      this.handleError = this.handleError.bind(this);
    }

    handleChange(event){
      var value = event.target.value;
      this.setState(function(){
        return {
          username: value
        }
      })
    }

    handlePageChange(event){
      var value = event.target.value;
      this.setState(function(){
        return {
          create: true
        }
      })
    }

    handlePasswordChange(event){
      var value = event.target.value;
      this.setState(function(){
        return {
          password: value
        }
      })
    }

    handleError(event){
      
    }

    handleSubmit(event){
      event.preventDefault();
        
      if (this.state.create) {
        axios.post('/user', {
              username: this.state.username,
              p: this.state.password,
            }).then((response) => {
              this.setState(function(){
                return {
                  create: false
                }
              })
              this.props.onSubmit(response.username);
            });
      } else {
        axios.post('/login', {
              username: this.state.username,
              p: this.state.password,
            }).then((response) => {
              this.setState(function(){
                return {
                  create: false
                }
              })
              this.props.onSubmit(response.username);
            });
      }
    }

    render() {
      if (!this.props.username) {
        return (
          <div>
          {this.state.create &&
            <form className='column' onSubmit={this.handleSubmit}>
              <h2>Sign Up</h2>
              <h4>{this.state.msg}</h4>
              <p><input
                id='username'
                className="rounded" 
                placeholder='username'
                type='text'
                
                autoComplete='off'
                value = {this.state.username}
                onChange={this.handleChange}
              /></p>

              <p>
                <input
                  id='password'
                  className="rounded" 
                  type='password'
                  placeholder='password'
                  autoComplete='off'
                  value = {this.state.password}
                  onChange={this.handlePasswordChange}
                />
              </p>
              
              <button
                className="button" 
                type='submit'
                disabled={!this.state.username || !this.state.password}
                 >
                  Submit
              </button>
            </form>
          }
          {!this.state.create &&
            <div>
            <form className='column' onSubmit={this.handleSubmit}>
              <h2>login</h2>
              <h4>{this.state.msg}</h4>
              <p><input
                id='username'
                className="rounded" 
                placeholder='username'
                type='text'
                
                autoComplete='off'
                value = {this.state.username}
                onChange={this.handleChange}
              /></p>

              <p>
                <input
                  id='password'
                  className="rounded" 
                  type='password'
                  placeholder='password'
                  autoComplete='off'
                  value = {this.state.password}
                  onChange={this.handlePasswordChange}
                />
              </p>

              <button
                className="button" 
                type='submit'
                disabled={!this.state.username}
                 >
                  Submit
              </button>
            </form>
            <br />
            <h5>Dont have an account? </h5>
            <form className='column' onSubmit={this.handlePageChange}>
              <button
                className="button" 
                type='change'
                onSubmit={this.handlePageChange}
                disabled={false}
                 >
                  Create an account
              </button>
            </form>
            </div>
          }
          </div>
        )
      } else {
        return (
          <h2>Successfully Logged In</h2>
        )
      }
    }
}

module.exports = Login;

   
