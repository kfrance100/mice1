var React = require('react');
var NavLink = require('react-router-dom').NavLink;

function Nav () {
  return (
    <ul className='nav'>
      <li>
        <NavLink exact activeClassName='active' to='/'>Select Data</NavLink>
      </li>
      {
      <li>
        <NavLink activeClassName='active' to='/mice1'>Mice 1</NavLink>
      </li>
      *
      <li>
        <NavLink activeClassName='active' to='/mice2'>Mice 2</NavLink>
      </li>
    </ul>
  )
}

module.exports = Nav;
