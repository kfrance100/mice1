var React = require('react');
var Link = require('react-router-dom').Link;

class Data extends React.Component {
  render() {
    return (
      <div>
        <title>Mice 1</title>
        <meta charSet="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="MiceCSS.css" type="text/css" rel="stylesheet" />
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
        <h1>Mice Data</h1>
        <br />        
        <div id="search">
          <h2>Search Database:</h2>
          <form>  
            <input type="checkbox" defaultValue="Purple" />Purple<br />
            <input type="checkbox" defaultValue="RED4" />Red4<br />
            <input type="checkbox" defaultValue="BLUE4" />Blue<br /><br />             
            Start Time:
            <input type="datetime-local" name="start" required /><br /><br />
            End Time:
            <input type="datetime-local" name="end" required /><br /><br />
            <input type="submit" />
          </form>
        </div>
      </div>
    );
  }
}

module.exports = Data;
