var React = require('react');


class FileInput extends React.Component {
  constructor(props) {
    // highlight-range{3}
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleUpload = this.handleSubmit.bind(this);
    this.fileInput = React.createRef();
    this.state = {
      req: '',
      res: null,
      file: null,
    };
  }
  handleChange(event) {
    this.setState(state => ({
      req: '',
      res: this.state.res,
      file: this.fileInput.current,
    }));
    
    console.log(this.state.file, this.fileInput.current, this.fileInput);
  }
  handleSubmit(event) {
    event.preventDefault();
    alert(
      `Selected file - ${
        this.fileInput.current.files[0].name
      }`
    );
  }
  handleUpload(event) {
    event.preventDefault();
    
  }

  render() {
    // highlight-range{5}
    return (
      <form onSubmit={this.handleSubmit}>
        <header>Upload a new dataset to use:</header>
        <br />
        <label>
          <input
            type="file"
            ref={(input) => this.fileInput = input}
            onChange={(e) => this.handleChange(e)} />
        </label>
        
        <br />
        
        <button type="submit" disabled={this.fileInput.current == null}>Upload</button>
      </form>
    );
  }
}


module.exports = FileInput;