import React, { Component } from 'react';
import './App.css';
var ReactRouter = require('react-router-dom');
var Router = ReactRouter.BrowserRouter;
var Route = ReactRouter.Route;
var Switch = ReactRouter.Switch;

// The "panel" component files to include
//var FileInput = require('./FileInput.js');
//var Nav = require('./Nav.js');
// Mice 1 files
var Login = require('./login.js');
// Mice 2 files
//var Options = require('./Mice 2/Options.js');


class App extends Component {
  render() {
    return (
      <Router>
      <div className="App">
        {/* Display the navigation bar */}
        <Nav />
        
        {/* Display the body of the page */}
        <Switch>
          
	  {/* Data Selection panel */}
          <Route exact path='/' render={function () {
            return (
              <div>
                <FileInput />
                
                <br />
                <br />
                <header>Or, select an existing dataset (not yet available):</header>
              </div>
            )
          }} />
          
          {/* Heat & Vector maps, and their display settings */}
          <Route path='/mice2' render={function () {
            return (
              <div>
                <Options />
                
                {/* Add area for the heatmap render */}
              </div>
            )
          }} />
          
          {/* Mice 1 files */}
          <Route path='/mice1' render={function () {
            return (
              <div>
                <Login />
                
                
              </div>
            )
          }} />
          

        </Switch>
      </div>
      </Router>
    );
  }
}

export default App;
