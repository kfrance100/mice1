var React = require('react');
var NavLink = require('react-router-dom').NavLink;

function Nav(props) {
  return (
    <ul className='nav'>
      <li>
        <NavLink exact activeClassName='active' to='/'>Home</NavLink>
      </li>
      <li>
        <NavLink activeClassName='active' to='/mice1/RFID'>RFID</NavLink>
      </li>
      <li>
	  <NavLink activeClassName='active' to='/mice1/data'>Data Input</NavLink>
      </li>
      <li>
        <NavLink activeClassName='active' to='/mice1/distanceGraph'>Distance Graph</NavLink>
      </li>
      <li>
        <NavLink activeClassName='active' to='/mice1/displayDistance'>Display Distance</NavLink>
      </li>
      <li>
        <NavLink activeClassName='active' to='/mice1/timeGraph'>Time Graph</NavLink>
      </li>
      <li>
        <NavLink activeClassName='active' to='/mice1/displayTime'>Display Time</NavLink>
      </li>
      <li>
	 <NavLink activeClassName='active' to='/mice1/prediction'>Prediction</NavLink>
      </li>
      <li>
        {!props.user && <NavLink activeClassName='active' to='/login'> Login</NavLink>}
      </li>
      <li>
        {props.user && <NavLink activeClassName='active' to='/logout'> Logout</NavLink>}
      </li>

      <li className="user">{props.user}</li>
    </ul>
  )
}

module.exports = Nav;
