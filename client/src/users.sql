create database users;
\c users

drop table users;


create table users (
	username varchar(15) not null unique,
	pwd varchar(15) not null,
	loggedin bool default false,
	zip int,
	primary key (username)
);

