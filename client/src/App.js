import React, { Component } from 'react';
import './App.css';
var ReactRouter = require('react-router-dom');
var Router = ReactRouter.BrowserRouter;
var Route = ReactRouter.Route;
var Switch = ReactRouter.Switch;

// The "panel" component files to include
//var FileInput = require('./FileInput.js');
var Nav = require('./nav.js');
var Home = require('./home.js');
var Data = require('./data.js');
var DisplayDistance = require('./displayDistance.js');
var DistanceGraph = require('./distanceGraph.js');
var DisplayTime = require('./displayTime.js');
var TimeGraph = require('./timeGraph.js');
var RFID = require('./rfid.js');
var Prediction = require('./prediction.js');

var Login = require('./login2.js');
var Logout = require('./logout.js');
var axios = require('axios');

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {username: ''};
    this.handleSubmit = this.handleSubmit.bind(this);
    this.logout = this.logout.bind(this);
  }

  handleSubmit(username) {
    this.setState(function() {
      console.log("handle submit");
      console.log(username);
      var newState = {
        username: username
      };
      return newState;
    });
  }

  logout(username) {
    console.log("Logging Out");
    
    axios.put('/user?item='+username).then((response) => {
      this.setState(function () {
        return {
          username: ''
        }
      });
    });
 
  }	
  render() {
    var username = this.state.username;
    
    return (
      <Router>
        <div className="App">
        {/* Display the navigation bar */}
          <Nav user={this.state.username}/> 
          
          <Switch>
            <Route exact path='/' component={Home} /> 
            <Route path='/login'  component={() => <Login onSubmit={this.handleSubmit} username={this.state.username}/> } /> 
            <Route path='/logout'  component={() => <Logout logout={this.logout} username={this.state.username} /> } /> 
            <Route path='/mice1/data' component={Data} />
	    <Route path='/mice1/distanceGraph' component={DistanceGraph} />
	    <Route path='/mice1/displayDistance' component={DisplayDistance} />
	    <Route path='/mice1/timeGraph' component={TimeGraph} />
	    <Route path='/mice1/displayTime' component={DisplayTime} />
	    <Route path='/mice1/RFID' component={RFID} />
	    <Route path='/mice1/prediction' component={Prediction} />

	    <Route render={
              function(){
                return <h1>Page Not Found</h1>
              }
            } /> 
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
