var React = require('react');
var Link = require('react-router-dom').Link;

class DisplayDistance extends React.Component {
  render() {
    return (
      <div>
        <title>Mice 1</title>
        <meta charSet="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="MiceCSS.css" type="text/css" rel="stylesheet" />
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
        <h1>Distance Graph</h1>
        <div id="myDiv" />
      </div>
    );
  }
}

module.exports = DisplayDistance;
