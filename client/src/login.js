var React = require('react');
var PropTypes = require('prop-types');
var axios = require('axios');

class login extends React.Component {
  constructor(props) {
    // highlight-range{3}
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleUpload = this.handleSubmit.bind(this);
    this.input = React.createRef();
    this.state = {
      req: '',
      res: null,
      file: null,
    };
  }
  handleChange(event) {
    this.setState(state => ({
      req: '',
      res: this.state.res,
      file: this.input.current,
    }));
    
    console.log(this.state.file, this.input.current, this.input);
  }
  handleSubmit(event) {
    event.preventDefault();
    alert(
      `Selected file - ${
        this.input.current.files[0].name
      }`
    );
  }
  handleUpload(event) {
    event.preventDefault();
    
  }

  render() {
    // highlight-range{5}
    return (
      <form onSubmit={this.handleSubmit}>
        <header>Login here</header>
        <br />
        <label>
          <input
            type="text"
            ref={(input) => this.input = input} />
        </label>
        
        <br />
        
        <button type="submit" disabled={this.input.current == null}>Upload</button>
      </form>
    );
  }
}


module.exports = login;
