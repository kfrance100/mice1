var React = require('react');


class VectorMap extends React.Component {
  // Returns JSX code that gets built into HTML content by React
  constructor(props){
    super(props);
    //this.state = {isToggleOn: true};
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state ={
        
      arr: [
        {name: "Build Heat Map", isToggleOn: true},

        {name: "Build Vector Map", isToggleOn: true},

        {name: "Reset Display", isToggleOn: true}
      ]
    };

    this.handleClick = this.handleClick.bind(this);
  }
  
  handleSubmit(e){
    alert('This is a test, Time Frame is: ' + this.input.value);
    e.preventDefault();
  }
  
  handleClick(index){//added index
    let tmp = this.state.arr; //added for multi
    tmp[index].isActive = !tmp[index].isActive;
    this.setState({arr: tmp});
    this.setState(state => ({
      isToggleOn: !state.isToggleOn
    }));

  }
  
  
  //end of addition
  render() {
    return (
      <div className='left-container'>
        <h3>Display some Plots!</h3>
        <p>
          Welcome to the visual page created by Mice 2 Group 2. Use the buttons below to view gathered data and please use this format while specifying a time frame: TIMEFRAME FORMAT HERE
        </p>

        {this.state.arr.map((el, index) =>
            <button key={index} handleClick={() => this.handleClick(index)}>   
              {el.name}
            </button>
          )}

          <form onSubmit={this.handleSubmit}>
          <label>
            Time Frame:
              <input type="text" ref={(input) => this.input = input} />
          </label>
            <input type="submit" value="Submit" />
          </form>




      </div>
    ); 
  }
}

// This must match the require statement in App.js
module.exports = VectorMap;
